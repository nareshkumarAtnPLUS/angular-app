import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent   {
  predictForm = this.fb.group({
    state:['',Validators.required],
    district:['',Validators.required],
    market:['',Validators.required],
    variety:['',Validators.required],
  })
  constructor(private fb: FormBuilder) { }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.predictForm.value);
  }


}